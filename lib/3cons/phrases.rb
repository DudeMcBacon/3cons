module ThreeCons
  #
  # A small class for determining how often three word consecutive phrases
  # occur in an arbitrary string of text ignoring puncation, case, and
  # newlines.
  #
  class Phrases
    # The string supplied to ThreeCons::Phrases on instantiation.
    # @return [String] the supplied string
    attr_accessor :string

    def initialize(string)
      @string = string
    end

    # Formats the string provided to ThreeCons::Phrases. Downcases the entire
    # string and removes any punctation that might be in it. Also transforms
    # newlines into spaces.
    #
    # @return [String] a string representing the formatted string
    def format
      @string.downcase
             .gsub(/[^a-z0-9\s]/i, '') # puncation
             .gsub(/\r?\n/, ' ')       # newlines
             .gsub(/  +/, ' ')         # double spaces
    end

    # Creates a hash representing the available three word consecutive
    # phrases and their occurences.
    #
    # @return [Hash] a hash with three word consecutive phrases and their
    #                occurances
    def phrases
      hash = {}
      format.split.each_cons(3) do |a|
        string = a.join(' ')
        hash[string] = if hash[string]
                         hash[string] + 1
                       else
                         1
                       end
      end
      hash
    end

    # Sorts the three word consecutive phrases in descending order by the
    # number of occurances in the supplied string.
    #
    # @return [Array] returns an array of arrays where the first element
    #                 of the inner array is the phrase and the second
    #                 element is the number of occurances in the supplied
    #                 string
    def sorted_phrases
      phrases.sort_by { |_k, v| v }.reverse
    end
  end
end
