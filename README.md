# 3cons
3cons is a simple utility for outputting a list of consecutive common three word phrases that occur in one or more text files.

## Installation
3cons is packaged as a traditional gem. ```Ruby 2.4``` was targeted when writing the code but it should be functional in any modern version of ruby. You can use bundler to execute it from the git repo once you've downloaded it:

```
$ bundle install
$ bundle exec bin/3cons --help
```

## Listing phrases
Listing phrases is simple. Find a text file you'd like to list phrases from:

```
$ bundle exec bin/3cons pg2009.txt
320 - of the same
126 - the same species
125 - conditions of life
116 - in the same
107 - of natural selection
103 - from each other
98 - species of the
89 - on the other
81 - the other hand
78 - the case of
[...snip...]
```

You can also pass text in from ```stdin```:

```
$ cat pg2009.txt | bundle exec bin/3cons
320 - of the same
126 - the same species
125 - conditions of life
116 - in the same
107 - of natural selection
103 - from each other
98 - species of the
89 - on the other
81 - the other hand
78 - the case of
[...snip...]
```

Several options are also available to control the output of the utility:

```
bundle exec bin/3cons --help
Lists common three consecutive word phrases from a file and their count

3cons 1.0.0
Options:
  -a, --all        List all the common phrases and their count
  -t, --top=<i>    List the top n common phrases and their count (default: 100)
  -v, --version    Print version and exit
  -h, --help       Show this message
```

## Running Tests
The tests for the program are written with ```rspec``` and can be run as follow:

```
$ bundle exec rspec

ThreeCons::Phrases
  #new
    should allow the string to be modified
  #format
    should be able to format strings with punctuation
    should be able to format strings new lines
    should be able to format strings with multiple spaces or newlines
    should be able to format strings with different capitalization
    should be able to format strings with all three
  #phrases
    should be able to return phrases and their occurances
  #sorted_phrases
    should be able to return a sorted of array of phrases and their occurances

Finished in 0.00278 seconds (files took 0.18227 seconds to load)
8 examples, 0 failures
```
