#!/usr/bin/env ruby
require '3cons/phrases'
require 'trollop'

# Let's get some command-line arguments in here.
#
# Note that Trollop will automatically cleanup ARGV so it's in a good state
# for the ARGF read below.
opts = Trollop.options do
  synopsis 'Lists common three consecutive word phrases from a file and their count'
  version "3cons #{ThreeCons::VERSION}"
  opt :all, 'List all the common phrases and their count', type: :boolean, default: false
  opt :top, 'List the top n common phrases and their count', type: :integer, default: 100
end

# From the rubydocs:
# "ARGF is a stream designed for use in scripts that process files given as
# command-line arguments or passed in via STDIN."
#
# ARGF assumes the values available in ARGV are filenames and allows us to read
# them automagically. Neat.
input = ARGF.read

# Instantiate a new ThreeCons
threecons = ThreeCons::Phrases.new(input)

# Print some common phrases!
limit = 0
threecons.sorted_phrases.each do |a|
  unless opts[:all]
    break if limit >= opts[:top]
  end
  puts "#{a[1]} - #{a[0]}"
  limit += 1
end
