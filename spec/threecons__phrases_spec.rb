require 'spec_helper'
require '3cons/phrases'

describe ThreeCons::Phrases do
  describe '#new' do
    it 'should allow the string to be modified' do
      threecons = ThreeCons::Phrases.new('these are words')
      expect(threecons.string).to eq('these are words')
      threecons.string = 'different words these are'
      expect(threecons.string).to eq('different words these are')
    end
  end

  describe '#format' do
    it 'should be able to format strings with punctuation' do
      threecons1 = ThreeCons::Phrases.new('^&*(^&(*I @#_)(*$@love &*)(&*)(*)&sandwiches.')
      threecons2 = ThreeCons::Phrases.new('(i)(*)(**) @$)(@*love @!@sandwiches!!)')
      expect(threecons1.format).to eq(threecons2.format)
    end

    it 'should be able to format strings new lines' do
      threecons1 = ThreeCons::Phrases.new("i love\nsandwiches")
      threecons2 = ThreeCons::Phrases.new('i love sandwiches')
      expect(threecons1.format).to eq(threecons2.format)
    end

    it 'should be able to format strings with multiple spaces or newlines' do
      threecons1 = ThreeCons::Phrases.new("i  love \nsandwiches")
      threecons2 = ThreeCons::Phrases.new('i love sandwiches')
      expect(threecons1.format).to eq(threecons2.format)
    end

    it 'should be able to format strings with different capitalization' do
      threecons1 = ThreeCons::Phrases.new('I lOvE sAnDwIcHeS')
      threecons2 = ThreeCons::Phrases.new('i love sandwiches')
      expect(threecons1.format).to eq(threecons2.format)
    end

    it 'should be able to format strings with all three' do
      threecons1 = ThreeCons::Phrases.new("I love\nsandwiches.")
      threecons2 = ThreeCons::Phrases.new('(I LOVE SANDWICHES!!)')
      expect(threecons1.format).to eq(threecons2.format)
    end
  end

  describe '#phrases' do
    it 'should be able to return phrases and their occurances' do
      string = %(
        this is a phrase
        This iS a phRase!
        this !!is a different phrase
        !this is an even different phrase
      )

      expected = {
        'this is a' => 3,
        'is a phrase' => 2,
        'a phrase this' => 2,
        'phrase this is' => 3,
        'is a different' => 1,
        'a different phrase' => 1,
        'different phrase this' => 1,
        'this is an' => 1,
        'is an even' => 1,
        'an even different' => 1,
        'even different phrase' => 1
      }

      threecons = ThreeCons::Phrases.new(string)
      expect(threecons.phrases).to eq(expected)
    end
  end

  describe '#sorted_phrases' do
    # FIXME: This test can fail when used with different versions of ruby. The
    # sorting algorithm might put phrases that share the same number of
    # occurances in a different order. Consider sorting the phrases that share
    # the same number of occurances alphabetically after the fact.
    it 'should be able to return a sorted of array of phrases and their occurances' do
      string = %(
        this is a phrase
        This iS a phRase!
        this !!is a different phrase
        !this is an even different phrase
      )

      expected = [
        ["this is a", 3],
        ["phrase this is", 3],
        ["a phrase this", 2],
        ["is a phrase", 2],
        ["a different phrase", 1],
        ["is a different", 1],
        ["an even different", 1],
        ["is an even", 1],
        ["this is an", 1],
        ["different phrase this", 1],
        ["even different phrase", 1]
      ]

      threecons = ThreeCons::Phrases.new(string)
      expect(threecons.sorted_phrases).to eq(expected)
    end
  end
end
