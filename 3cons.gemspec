lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require '3cons'

Gem::Specification.new do |spec|
  spec.name          = '3cons'
  spec.version       = ThreeCons::VERSION
  spec.authors       = ['Brandon Burnett']
  spec.email         = ['gentoolicious@gmail.com']

  spec.summary       = 'A small class to parse common phrases out of a long string.'
  spec.description   = 'Given a string of arbitrary size you can use this gem to parse out all of the three consecutive word phrases and determine which occur the most often.'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop'
  spec.add_dependency 'trollop'
end
